#!/usr/bin/env bash

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../commons/commons.sh"

logging:create() {
	local cluster_name=${1}
        ${KUBECTL_CMD} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/fluentd-serviceaccount.yaml" #&>dev/null
	${KUBECTL_CMD} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/fluentd-config.yaml" #&>dev/null
	${KUBECTL_CMD} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/fluentd-env-config.yaml" #&>dev/null
		cat <<EOF | ${KUBECTL_CMD} apply -f -
kind: ConfigMap
apiVersion: v1
metadata:
  name: fluentd-env-config
  namespace: logging
data:
  FLUENT_UID: "0"
  FLUENT_ELASTICSEARCH_SED_DISABLE: "true"
  CLUSTER_NAME: "${cluster_name}"
EOF

	${KUBECTL_CMD} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/fluentd-daemonset.yaml" #&>dev/null

	printf "${RED}Secret for Fluent needs to be created manually in logging namespace!${NC}\n"
}
