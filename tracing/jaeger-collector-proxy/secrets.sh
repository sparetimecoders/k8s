#!/usr/bin/env bash
set -euo pipefail

source ${BUILD_TOOLS_PATH}/scripts/kubernetes.sh

ENVIRONMENT="${1?usage: secrets.sh <environment> <jaeger environment>}"
JAEGER_ENV="${2?usage: secrets.sh <environment> <jaeger environment>}"
ITEM_NAME=jaeger-collector
SECRET_NAME=jaeger-collector-proxy-config

kube_cmd=$(kubernetes:get_command ${ENVIRONMENT})

UUID=$(op list items | \
	jq -r \
	--arg item "$ITEM_NAME" \
	--arg environment "env=$JAEGER_ENV" \
	'.[] | select(.overview.title == $item) | select(.overview.tags as $t | $environment | IN($t[])) | .uuid')

ITEM=$(op get item "$UUID" | jq '{"url": .overview.url, "fields": .details.fields}')
AUTH=$(echo "$ITEM" | \
	jq -c '.fields[] | {"\(.name)": .value}' | \
	jq -r -s 'reduce .[] as $item ({}; . + $item) | "\(.username):\(.password)"' | \
	tr -d '\n' | \
	base64 -w0)
URL=$(echo "$ITEM" | jq -r '.url')
shopt -s extglob
HOST_VALUE=${URL#http?(s)://}

CONTENT=$(export AUTH="$AUTH"; export URL="$URL"; export HOST_VALUE="$HOST_VALUE"; envsubst '${AUTH} ${URL} ${HOST_VALUE}' < default.conf)

$kube_cmd delete secret -n tracing ${SECRET_NAME,,} &> /dev/null || true
echo "$CONTENT" | $kube_cmd create secret generic -n tracing \
  ${SECRET_NAME,,} \
  --from-file=default.conf=/dev/stdin
