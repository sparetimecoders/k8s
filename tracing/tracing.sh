#!/usr/bin/env bash

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../commons/commons.sh"

tracing:create() {
  # TODO Handle prod/dev config
  # TODO Secret for password and ENV variable
  local NAMESPACE=tracing
  ${KUBECTL_CMD} create namespace ${NAMESPACE}
  ${KUBECTL_CMD} --namespace ${NAMESPACE} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/jaeger-collector-config.yaml" #&>dev/null
  ${KUBECTL_CMD} --namespace ${NAMESPACE} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/jaeger-collector-secret.yaml" #&>dev/null
  ${KUBECTL_CMD} --namespace ${NAMESPACE} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/jaeger.yaml" #&>dev/null
}
