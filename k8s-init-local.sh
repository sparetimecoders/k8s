#!/usr/bin/env bash
set -euo pipefail

local:helm() {
  # Setup Helm
  helm --kube-context ${CONTEXT} init
  # Wait for tiller to be setup
  set +eo pipefail

  printf "Waiting for helm tiller service to startup.."
  while [[ ! $(${KUBECTL_CMD} --namespace kube-system get pods | grep tiller | grep Running | grep 1/1 ) ]]
  do
    printf "."
    sleep 1
  done
  printf "done\n"
  set -eo pipefail
}

local:init() {
  local:helm
}
