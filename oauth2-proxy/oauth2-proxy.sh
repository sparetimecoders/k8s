#!/usr/bin/env bash

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../commons/commons.sh"

oauth2-proxy:create() {
  local NAMESPACE=oauth2-proxy
  local FILE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/oauth2-proxy.yaml"

  ${KUBECTL_CMD} create namespace ${NAMESPACE}

  AZURE_AD_TENANT_ID="${1}" \
  OAUTH2_PROXY_CLIENT_ID="${2}" \
  OAUTH2_PROXY_CLIENT_SECRET="${3}" \
  OAUTH2_PROXY_COOKIE_SECRET="${4}" \
  envsubst < ${FILE} | ${KUBECTL_CMD} apply --record=false -f -
}
